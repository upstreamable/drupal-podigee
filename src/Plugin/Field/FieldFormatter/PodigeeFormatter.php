<?php

namespace Drupal\podigee\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Plugin\Field\FieldFormatter\FileMediaFormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'file_audio' formatter.
 *
 * @FieldFormatter(
 *   id = "podigee",
 *   label = @Translation("Podigee Podcast Player"),
 *   description = @Translation("Display the file using Podigee HTML5 player."),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class PodigeeFormatter extends FileMediaFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'title' => '',
      'subtitle' => '',
      'description' => '',
      'image' => '',
      'podcast_title' => '',
      'podcast_feed' => '',
      'theme' => 'default',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $parent_form = parent::settingsForm($form, $form_state);
    // Not supported.
    unset($parent_form['controls']);
    unset($parent_form['autoplay']);
    unset($parent_form['loop']);
    unset($parent_form['multiple_file_display_type']);

    return [
      'podcast_title' => [
        '#title' => $this->t('Podcast title'),
        '#description' => $this->t('Tokens can be used for example [media:name]'),
        '#type' => 'textfield',
        '#default_value' => $this->getSetting('podcast_title'),
      ],
      'podcast_feed' => [
        '#title' => $this->t('Podcast feed'),
        '#description' => $this->t('Tokens can be used for example [media:name]'),
        '#type' => 'textfield',
        '#default_value' => $this->getSetting('podcast_feed'),
      ],
      'title' => [
        '#title' => $this->t('Title'),
        '#description' => $this->t('Tokens can be used for example [media:name]'),
        '#type' => 'textfield',
        '#default_value' => $this->getSetting('title'),
      ],
      'subtitle' => [
        '#title' => $this->t('Subtitle'),
        '#description' => $this->t('Tokens can be used for example [media:name]'),
        '#type' => 'textfield',
        '#default_value' => $this->getSetting('subtitle'),
      ],
      'description' => [
        '#title' => $this->t('Description'),
        '#description' => $this->t('Tokens can be used for example [media:name]'),
        '#type' => 'textfield',
        '#default_value' => $this->getSetting('description'),
      ],
      'image' => [
        '#title' => $this->t('Image'),
        '#description' => $this->t('Tokens can be used for example [media:image]'),
        '#type' => 'textfield',
        '#default_value' => $this->getSetting('image'),
      ],
      'theme' => [
        '#title' => $this->t('Theme'),
        '#description' => $this->t('Use a different theme for the player', []),
        '#type' => 'select',
        '#options' => $this->getThemeOptions(),
        '#default_value' => $this->getSetting('theme'),
      ],
    ] + $parent_form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Theme: %theme', ['%theme' => $this->getThemeOptions()[$this->getSetting('theme')]]);
    return $summary;
  }

  /**
   * Themes supported.
   */
  protected function getThemeOptions() {
    return [
      'default' => $this->t('Default'),
      'default-dark' => $this->t('Default dark'),
      'minimal' => $this->t('Minimal'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function getMediaType() {
    return 'audio';
  }

  /**
   * Replace tokens for the current entity.
   */
  protected function replace($setting_name, $entity) {
    $token = \Drupal::service('token');
    return $token->replace(
      $this->getSetting($setting_name),
      [$entity->getEntityTypeId() => $entity],
      ['clear' => TRUE]
    );
  }

  /**
   * Gets source files with attributes.
   *
   * @param \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items
   *   The item list.
   * @param string $langcode
   *   The language code of the referenced entities to display.
   *
   * @return array
   *   Numerically indexed array, which again contains an associative array with
   *   the following key/values:
   *     - file => \Drupal\file\Entity\File
   *     - source_attributes => \Drupal\Core\Template\Attribute
   */
  protected function getSourceFiles(EntityReferenceFieldItemListInterface $items, $langcode) {
    $source_files = [];
    $entity = $items->getEntity();

    // Because we can have the files grouped in a single media tag, we do a
    // grouping in case the multiple file behavior is not 'tags'.
    /** @var \Drupal\file\Entity\File $file */
    foreach ($this->getEntitiesToView($items, $langcode) as $file) {
      if (static::mimeTypeApplies($file->getMimeType())) {
        $player_configuration = [
          'options' => [
            'theme' => $this->getSetting('theme'),
            'sslProxy' => 'https://cdn.podigee.com/ssl-proxy/',
          ],
          'podcast' => [
            'title' => $this->replace('podcast_title', $entity),
            'feed' => $this->replace('podcast_feed', $entity),
          ],
          'episode' => [
            'media' => [
              // TODO: Support other file types than mp3.
              'mp3' => file_create_url($file->getFileUri()),
            ],
            'title' => $this->replace('title', $entity),
            'subtitle' => $this->replace('subtitle', $entity),
            'description' => $this->replace('description', $entity),
            'coverUrl' => $this->replace('image', $entity),
          ],
        ];
        $hash = hash('sha256', json_encode($player_configuration));
        if ($this->getSetting('multiple_file_display_type') === 'tags') {
          $source_files[] = [
            [
              'file' => $file,
              'player_configuration' => $player_configuration,
              'hash' => $hash,
            ],
          ];
        }
        else {
          $source_files[0][] = [
            'file' => $file,
            'player_configuration' => $player_configuration,
            'hash' => $hash,
          ];
        }
      }
    }
    return $source_files;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    foreach ($elements as &$element) {
      foreach ($element['#files'] as $file) {
        $element['#attached']['library'][] = 'podigee/podigee';
        $element['#attached']['drupalSettings']['podigee'][$file['hash']] = $file['player_configuration'];
      }
    }
    return $elements;
  }

}
