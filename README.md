# Podigee podcast player

A field formatter for audio files implementing the
[Podigee podcast player](http://podigee.github.io/podigee-podcast-player/)

## Features

* Show a cover image for the podcast from the same media entity using tokens (same for title, description and subtitle)
* Show episodes from a rss feed in the players.
* Ability to download the podcast and share it on social networks
* Choose between the three available themes.

Check [the project out](https://github.com/podigee/podigee-podcast-player) to see what is possible to do.

## Installation

It is recommended to install this module using composer.

```
composer require drupal/podigee:^1@alpha
```

Have look at
[Composer template for Drupal projects](https://github.com/drupal-composer/drupal-project)
if you are not familiar on how to manage Drupal projects with composer.

## Alpha stage

These items need to be resolved to get the module out of alpha (patches are welcome):

 * Only mp3 files are supported
 * Only one episode for each player

## Contributions

Patches on drupal.org are accepted but merge requests on
[gitlab](https://gitlab.com/upstreamable/drupal-podigee) are preferred.

## Real time communication

You can join the [#podigee](https://drupalchat.me/channel/podigee)
channel on [drupalchat.me](https://drupalchat.me).
