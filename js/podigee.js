/**
 * @file
 * Podigee behaviors.
 */

(function (Drupal) {

  'use strict';

  /**
   * Take the configuration from the settings object and place it in a top level variable.
   *
   * This due to limitations of the Podigee Player.
   */
  Drupal.behaviors.podigee = {
    attach: function (context, settings) {
      Object.entries(settings.podigee).forEach(entry => {
        var key = entry[0];
        var value = entry[1];
        value.episode.url = window.location.href;
        window['playerConfiguration' + key] = value;
        delete settings.podigee[key];
        var script = document.getElementById('podigee-placeholder-' + key);
        script.setAttribute('id', null);
        script.setAttribute('class', 'podigee-podcast-player');
        script.setAttribute('data-configuration', 'playerConfiguration' + key);
        script.setAttribute('src', 'https://cdn.podigee.com/podcast-player/javascripts/podigee-podcast-player.js');
      });
    }
  };
} (Drupal));
